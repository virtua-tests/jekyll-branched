# [GitLab Pages] - Branched

There's just this file in the `master` branch and the pages live in the `pages` branch.

This way, you can have your project's code in the `master` branch and use an orphan branch (`pages`) for hosting your site. Learn more about it [here](http://doc.gitlab.com/ee/pages/README.html#how-to-set-up-gitlab-pages-in-a-repository-where-theres-also-actual-code).

View Pages: https://gitlab.com/pages/jekyll-branched/tree/pages

View site: https://pages.gitlab.io/jekyll-branched/

[GitLab Pages]: https://pages.gitlab.io
